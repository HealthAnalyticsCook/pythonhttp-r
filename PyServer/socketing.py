import socket

host = socket.gethostname() # Get local machine name

class Server:

    def __init__(self, host=host, port=8080):

        self.Sock = socket.socket()
        self.Host = host
        self.Port = port
        self.Sock.bind((self.Host, self.Port))

    def goLive(self, handshake, reaction):
        self.Sock.listen()
        while True:
            incomingConn, addr = self.Sock.accept()  # Establish connection with client.
            handshake(incomingConn, addr) # Code to run on connection
            data = incomingConn.recv(1080)
            if data.decode() != '':
                reaction(incomingConn, addr, data) # code to run when a message comes in


def handshake(incomingConn, addr):
    print('Got connection from', addr)
    incomingConn.send('Thank you for connecting'.encode())

def reaction(incomingConn, addr, data):
    print(data.decode())
    returnMessage = data.decode()
    incomingConn.send(returnMessage.encode())
    incomingConn.close()

server = Server()
server.goLive(handshake, reaction)
