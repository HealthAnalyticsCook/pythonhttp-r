import unittest
import socket

class ServerTest(unittest.TestCase):

    def setUp(self):
        self.Host = socket.gethostname()
        self.Port = 8080
        self.Sock = socket.socket()

    def openConection(self):
        self.Sock.connect((self.Host, self.Port))
        msg = self.Sock.recv(1080)

    def test_serverUp(self):
        try:
            self.openConection()
            self.Sock.close()
        except ConnectionRefusedError:
            assert False

    def test_communicate(self):
        try:
            self.openConection()
            self.Sock.send("Message".encode())
            msg = self.Sock.recv(1080)
            self.Sock.close()
        except ConnectionRefusedError:
            assert False

    def test_response(self):
        try:
            self.openConection()
            sendmsg = "Message"
            self.Sock.send(sendmsg.encode())
            msg = self.Sock.recv(1080)
            self.Sock.close()
            self.assertEqual(sendmsg, msg.decode())
        except ConnectionRefusedError:
            assert False

if __name__ == '__main__':
    unittest.main()

